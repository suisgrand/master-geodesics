﻿using System;
using Geodesics.Domain.AggregatesModel;
using Geodesics.Domain.SeedWork;
using Geodesics.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace Geodesics.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DistanceController : ControllerBase
    {
        private const double PointOriginDegrees = 90;
        private const double HalfCircleDegrees = 180.0;

        public IGeodesicFactory _geodesicFactory { get; }

        public DistanceController(IGeodesicFactory _geodesicFactory)
        {
            this._geodesicFactory = _geodesicFactory;

        }

        /// <summary>
        /// Retrieves the distance between the provided points.
        /// </summary>
        /// <response code="200">Distance successfully calculated.</response>
        /// <response code="400">Latitude or longitude of any of the given points is out of range.</response>
        /// <response code="500">Unexpected server exception.</response>
        [HttpGet]
        [Route("{distanceMethod}/{measureUnit}")]
        public ActionResult<DistanceResponse> Get(DistanceMethod distanceMethod, MeasureUnit measureUnit,
            [FromQuery] double point1Latitude, [FromQuery] double point1Longitude,
            [FromQuery] double point2Latitude, [FromQuery] double point2Longitude)
        {
            
            //wrapping the controller get request with a try/catch statement in order to 
            //send a 400 error request, if any errors on the data
            try
            {
                var geodesic = _geodesicFactory.GetGeodesic(point1Latitude, point1Longitude,
                point2Latitude, point2Longitude, measureUnit, distanceMethod);
                //sending the data to the infrastructure layer. this layer will in turn 
                //call in the domain model, in order to separate the API layer from the domain
                //layer
                var result = geodesic.Calculate();

                return new ActionResult<DistanceResponse>(new DistanceResponse
                {
                    Distance = result
                }); 
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }


}
