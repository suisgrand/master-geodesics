namespace Geodesics.Domain.AggregatesModel
{
    public static class Constants
    {
        public static readonly double MinimumLatitude = -90;
        public static readonly double MaximumLatitude = 90;
        
        public static readonly double MinimumLongitude = -180;
        public static readonly double MaximumLongitude = 180;
        
        public static readonly string ErrorRangeLongitude = 
            $"Must be in [{Constants.MinimumLongitude}, {Constants.MaximumLongitude}] interval";
        public static readonly string ErrorRangeLatitude = 
            $"Must be in [{Constants.MinimumLatitude}, {Constants.MaximumLatitude}] interval";

        public static readonly double PointOriginDegrees = 90;
        public static readonly double HalfCircleDegrees = 180.0;

        public static readonly int EarthRadiusInKm = 6371;
        public static readonly int EarthRadiusInMiles = 3959;
    }
}