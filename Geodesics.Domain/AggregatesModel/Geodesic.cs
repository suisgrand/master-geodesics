using Geodesics.Domain.SeedWork;
using Geodesics.Domain.Services;
using System;

namespace Geodesics.Domain.AggregatesModel
{

    //i moved the calculation geodesic calculation from the controller to the
    //domain model. 
    
    public sealed class Geodesic : IGeodesic
    {
        //setting all the properites to be private on the domain model in order to conform to 
        //DDD principles
        public DistancePoint Coordinate1 { get; private set; }
        public DistancePoint Coordinate2 { get; private set; }
        public MeasureUnit MeasureUnit { get; private set; }
        public DistanceMethod DistanceMethod { get; private set; }

        public Geodesic(double latitudePoint1, double longitudePoint1,
        double latitudePoint2, double longitudePoint2, MeasureUnit unit, DistanceMethod distanceMethod)
        {
            this.Coordinate1 = new DistancePoint(latitudePoint1, longitudePoint1);
            this.Coordinate2 = new DistancePoint(latitudePoint2, longitudePoint2);
            this.MeasureUnit = unit;
        }

        public double Calculate()
        {
            switch (this.DistanceMethod)
            {
                case DistanceMethod.GeodesicCurve:
                    return CalculateGeodesicCurve();
                case DistanceMethod.Pythagoras:
                    return CalculatePythagoras();
                default:
                    throw new ArgumentOutOfRangeException(nameof(this.DistanceMethod), this.DistanceMethod, null);
            }
        }

        private double CalculateGeodesicCurve()
        {

            var pointDegreeMinusLatitude1 = 
                Constants.PointOriginDegrees - this.Coordinate2.Latitude;
            var pointDegreeMinusLatitude2 = 
                Constants.PointOriginDegrees - this.Coordinate1.Latitude;
            var longitudeDifference = this.Coordinate1.Longitude - this.Coordinate2.Longitude;

            var cosP = Math.Cos(pointDegreeMinusLatitude1.GetDegreesToRadians()) * 
                        Math.Cos(pointDegreeMinusLatitude2.GetDegreesToRadians()) +
                        Math.Sin(pointDegreeMinusLatitude1.GetDegreesToRadians()) * 
                        Math.Sin(pointDegreeMinusLatitude2.GetDegreesToRadians()) * 
                        Math.Cos(longitudeDifference.GetDegreesToRadians());
            var radiusToDegree = Math.Acos(cosP).GetRadiansToDegrees();
            var result = Math.PI * radiusToDegree * this.MeasureUnit.GetEarthRadius() / 180;
            return result;
        }

        private double CalculatePythagoras()
        {
            var subCalcul1 = (this.Coordinate2.Longitude.GetDegreesToRadians() - 
                    this.Coordinate1.Longitude.GetDegreesToRadians()) *
                    Math.Cos((this.Coordinate1.Latitude.GetDegreesToRadians() + 
                    this.Coordinate2.Latitude.GetDegreesToRadians()) / 2);
            var subCalcul2 = this.Coordinate2.Latitude.GetDegreesToRadians() - this.Coordinate1.Latitude.GetDegreesToRadians();
            var result = Math.Sqrt(subCalcul1 * subCalcul1 + subCalcul2 * subCalcul2) * 
                this.MeasureUnit.GetEarthRadius();
            return result;
        }
    }
}