using Geodesics.Domain.SeedWork;


namespace Geodesics.Domain.AggregatesModel
{
    public interface IGeodesic
    {
        DistancePoint Coordinate1 { get; }
        DistancePoint Coordinate2 { get; }
        MeasureUnit MeasureUnit { get; }
        DistanceMethod DistanceMethod { get;  }

        double Calculate();
    }
}