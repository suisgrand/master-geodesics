namespace Geodesics.Domain.SeedWork
{
    public enum DistanceMethod
    {
        GeodesicCurve,
        Pythagoras
    }
}