using System;
using Geodesics.Domain.AggregatesModel;

namespace Geodesics.Domain.SeedWork
{
    public sealed class DistancePoint
    {
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }

        public DistancePoint(double latitude, double longitude)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;

            ValidatePoint();
        }

        private void ValidatePoint()
        {
            if (this.Latitude < Constants.MinimumLatitude || 
                this.Latitude > Constants.MaximumLatitude)
            {
                throw new ArgumentOutOfRangeException(nameof(this.Latitude), 
                    Constants.ErrorRangeLatitude);
            }
            if (this.Longitude < Constants.MinimumLongitude || 
                this.Longitude > Constants.MaximumLongitude)
            {
                throw new ArgumentOutOfRangeException(nameof(this.Longitude), 
                Constants.ErrorRangeLongitude);
            }
        }
    }
}