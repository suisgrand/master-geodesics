namespace Geodesics.Domain.SeedWork
{
    public enum MeasureUnit
    {
        Km,
        Mile
    }
}