using System;
using Geodesics.Domain.AggregatesModel;
using Geodesics.Domain.SeedWork;

namespace Geodesics.Domain.Services
{
    public static class Extensions
    {
        // i created these extension methods below since these calculations are 
        //used multiple times
        public static double GetRadiansToDegrees(this double radians)
        {
            return radians * Constants.HalfCircleDegrees / Math.PI;
        }

        public static double GetDegreesToRadians(this double degrees)
        {
            return degrees * Math.PI / Constants.HalfCircleDegrees;
        }

        public static double GetEarthRadius(this MeasureUnit measureUnit)
        {
            switch (measureUnit)
            {
                case MeasureUnit.Km:
                    return Constants.EarthRadiusInKm;
                case MeasureUnit.Mile:
                    return Constants.EarthRadiusInMiles;
                default:
                    throw new ArgumentOutOfRangeException(nameof(measureUnit));
            }
        }
    }
}