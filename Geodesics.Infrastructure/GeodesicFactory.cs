﻿using System;
using Geodesics.Domain.AggregatesModel;
using Geodesics.Domain.SeedWork;

namespace Geodesics.Infrastructure
{

    public class GeodesicFactory : IGeodesicFactory
    {
        
        public double LatitudePoint1 { get; set; }
        public double LongitudePoint1 { get; set; }
        public double LatitudePoint2 { get; set; }
        public double LongitudePoint2 { get; set; }
        public MeasureUnit Unit { get; set; }
        public DistanceMethod distanceMethod { get; set; }

        public IGeodesic GetGeodesic(
            double latitudePoint1,
            double longitudePoint1,
            double latitudePoint2,
            double longitudePoint2,
            MeasureUnit unit,
            DistanceMethod distanceMethod)
        {
            //using this layer in order to create a new domain model
            return new Geodesic(latitudePoint1, longitudePoint1,
                latitudePoint2, longitudePoint2, unit, distanceMethod);
        }

        public void CreateGeodesic()
        {
            //https://stackoverflow.com/questions/31950362/factory-method-with-di-and-ioc
        }
    }
}
