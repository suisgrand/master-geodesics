using Geodesics.Domain.AggregatesModel;
using Geodesics.Domain.SeedWork;

namespace Geodesics.Infrastructure
{
    public interface IGeodesicFactory
    {
        IGeodesic GetGeodesic(double latitudePoint1, double longitudePoint1, 
        double latitudePoint2, double longitudePoint2, MeasureUnit unit, DistanceMethod distanceMethod);
    }
}
