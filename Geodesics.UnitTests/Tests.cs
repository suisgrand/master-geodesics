using Geodesics.Api.Controllers;
using Geodesics.Domain.AggregatesModel;
using Geodesics.Domain.SeedWork;
using Geodesics.Infrastructure;
using NUnit.Framework;

using System;

namespace Geodesics.UnitTests
{
    [TestFixture]
    public class Tests
    {
        
        
        [TestCase(DistanceMethod.GeodesicCurve, 90, -6.372663, 90, -81.440440, MeasureUnit.Km, 0, false)]
        [TestCase(DistanceMethod.GeodesicCurve, 53.297975, -6.372663, 41.385101, -81.440440, MeasureUnit.Km, 5536.33868226669, false)]
        [TestCase(DistanceMethod.GeodesicCurve, 40.05, -6.372663, 41.385101, -81.440440, MeasureUnit.Km, 6117.16757569389, false)]
        [TestCase(DistanceMethod.GeodesicCurve, 200, -6.372663, 41.385101, -81.440440, MeasureUnit.Km, 0, true)]
        //on the test above, the latitude value is too high, and will cause the DistancePoint value object to 
        //throw an error
        public void Check_That_Geodesic_AggregateRoot_Can_Calcuate(DistanceMethod distanceMethod,
            double latPoint1, double lngPoint1, 
            double latPoint2, double lngPoint2, 
            MeasureUnit unit, double resultExpected, bool isThrowingException)
        {
            
            if (isThrowingException)
            {
                //arrange
                //act
                //assert
                Assert.That(() => new Geodesic(latPoint1, lngPoint1, latPoint2, lngPoint2, unit, distanceMethod),
                    Throws.Exception.TypeOf<ArgumentOutOfRangeException>());
            }
            else{
                //arrange
                var geodesic = new Geodesic(latPoint1, lngPoint1, latPoint2, lngPoint2, unit, distanceMethod);
                //act
                var result = geodesic.Calculate();
                //assert
                Assert.That(Math.Abs(result - resultExpected) < 0.0000001);
            }            
        }


        [TestCase(DistanceMethod.GeodesicCurve, 53.297975, -6.372663, 41.385101, -81.440440, MeasureUnit.Km, 5536.33868226669, false)]
        public void Controller_Test(DistanceMethod distanceMethod, double latPoint1, double lngPoint1, 
            double latPoint2, double lngPoint2, 
            MeasureUnit unit, double resultExpected, bool isThrowingException)
        {
            //Arrange
            var geodesicRequst = new GeodesicFactory();
            var controller = new DistanceController(geodesicRequst);

            //Act
            var resultAction = controller.Get(distanceMethod, unit, latPoint1, lngPoint1,
                latPoint2, lngPoint2);
            var result = resultAction.Value;

            //Assert
            Assert.That(Math.Abs(result.Distance - resultExpected) < 0.0000001);
        }

    }
}